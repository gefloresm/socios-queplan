import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

// Services
import { SolicitudesService } from '../../../@core/data/solicitudes.service';

@Component({
  selector: 'ngx-detalle-solicitud',
  templateUrl: './detalle-solicitud.component.html',
  styleUrls: ['./detalle-solicitud.component.scss'],
})
export class DetalleSolicitudComponent implements OnInit {

  status: any[] = [
    { id: 1, name: 'Sin Estado', color: 'secondary'},
    { id: 2, name: 'Asignada', color: 'progreso'},
    { id: 3, name: 'Cerrada', color: 'close'},
    { id: 4, name: 'Fallida', color: 'danger'},
    { id: 5, name: 'Eliminada', color: 'danger'},
    { id: 6, name: 'Duplicada', color: 'danger'},
    { id: 97, name: 'Pruebas QP', color: 'danger'},
    { id: 98, name: 'Cliente Falso', color: 'danger'},
    { id: 99, name: 'Otro', color: 'danger'},
  ];

  solicitud: any;

  constructor(
    private solicitudesService: SolicitudesService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
       const id = +params['id'];
       this.solicitudesService
         .get(id)
         .subscribe((solicitud: any) => this.solicitud = solicitud.data);
    });
  }

  getStatus(status: number): string {
    if (!status) return '';

    return (this.status.find(value => value.id === status) || { name: 'Sin Estado' }).name;

  }

  getClass(value, type = 'bg-'): string {
    return type + (this.status.find(x => x.id === value) || { color: 'secondary' }).color;
  }

}
