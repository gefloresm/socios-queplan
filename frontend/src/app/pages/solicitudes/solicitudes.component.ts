import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';

// Components
import { DatatableComponent } from '@swimlane/ngx-datatable';

// Services
import { SolicitudesService } from '../../@core/data/solicitudes.service';

@Component({
  selector: 'ngx-solicitudes',
  templateUrl: './solicitudes.component.html',
  styleUrls: ['./solicitudes.component.scss'],
})
export class SolicitudesComponent implements OnInit, OnDestroy {

  @ViewChild(DatatableComponent) table: DatatableComponent;

  private alive = true;

  rows: any[] = [];
  columns = [];
  total: number;
  temp: any[] = [];

  status: any[] = [
    { id: 1, name: 'Sin Estado', color: 'secondary'},
    { id: 2, name: 'Asignada', color: 'progreso'},
    { id: 3, name: 'Cerrada', color: 'close'},
    { id: 4, name: 'Fallida', color: 'danger'},
    { id: 5, name: 'Eliminada', color: 'danger'},
    { id: 6, name: 'Duplicada', color: 'danger'},
    { id: 97, name: 'Pruebas QP', color: 'danger'},
    { id: 98, name: 'Cliente Falso', color: 'danger'},
    { id: 99, name: 'Otro', color: 'danger'},
  ];

  constructor(
    private solicitudesService: SolicitudesService,
  ) { }

  ngOnInit() {
    this.solicitudesService.query().subscribe((solicitudes: any) => {
      this.temp = [...solicitudes];
      this.rows = solicitudes;
      this.total = solicitudes.length;
    });
  }


  getStatus(status): string {
    return (this.status.find(value => value.id === status) || { name: 'Sin Estado' }).name;

  }

  getClass(value, type = 'badge-'): string {
    return type + (this.status.find(x => x.id === value) || { color: 'secondary' }).color;
  }

  ngOnDestroy() {
    this.alive = false;
  }

  onDeleteConfirm(event): void {
    if (window.confirm('¿Seguro que deseas eliminar este usuario?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  filter(event) {
    const val = event.target.value.toLowerCase();

    const temp = this.temp.filter(function(d) {
      return (d.nombre_persona || '').toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.rows = temp;
    this.table.offset = 0;
  }
}
