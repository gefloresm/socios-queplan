import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SociosComponent } from './socios/socios.component';
import { SolicitudesComponent } from './solicitudes/solicitudes.component';
import { DetalleSolicitudComponent } from './solicitudes/detalle-solicitud/detalle-solicitud.component';
import { DetalleSocioComponent } from './socios/detalle-socio/detalle-socio.component';
import { ContactosComponent } from './contactos/contactos.component';
import { SaldosYMovimientosComponent } from './saldos-y-movimientos/saldos-y-movimientos.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'solicitudes',
      component: SolicitudesComponent,
    },
    {
      path: 'solicitud/:id',
      component: DetalleSolicitudComponent,
    },
    {
      path: 'socios',
      component: SociosComponent,
    },
    {
      path: 'socio/:id',
      component: DetalleSocioComponent,
    },
    {
      path: 'contactos',
      component: ContactosComponent,
    },
    {
      path: 'saldos-y-movimientos',
      component: SaldosYMovimientosComponent,
    },
    {
      path: 'dashboard',
      component: DashboardComponent,
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
