import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

// Services
import { SociosService } from '../../../@core/data/socios.service';

@Component({
  selector: 'ngx-detalle-socio',
  templateUrl: './detalle-socio.component.html',
  styleUrls: ['./detalle-socio.component.scss'],
})
export class DetalleSocioComponent implements OnInit {

  socio: any;

  constructor(
    private sociosService: SociosService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
       const id = +params['id'];
       this.sociosService
         .get(id)
         .subscribe((socio: any) => this.socio = socio.data);
    });
  }
}
