import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
// Components
import { DatatableComponent } from '@swimlane/ngx-datatable';

// Services
import { ContactosService } from '../../@core/data/contactos.service';


@Component({
  selector: 'ngx-contactos',
  templateUrl: './contactos.component.html',
  styleUrls: ['./contactos.component.scss'],
})
export class ContactosComponent implements OnInit, OnDestroy {

  @ViewChild(DatatableComponent) table: DatatableComponent;
  private alive = true;

  rows: any[] = [];
  columns = [
    { prop: '_id', name: 'ID' },
    { prop: 'estado_solicitud', name: 'Origen' },
    { prop: 'nombre', name: 'Nombre' },
    { prop: 'telefono', name: 'Teléfono' },
    { prop: 'email', name: 'Email' },
    { prop: 'asignado_por', name: 'Asignado A' },
    { prop: 'notas_admin', name: 'Estado Contacto' },
    { prop: 'asignado_a', name: 'Notas Admin' },
    { prop: 'fecha', name: 'Fecha' },
    { prop: 'mensaje', name: 'Mensaje' },
    { prop: 'nombre_persona', name: 'Datos Ingresados' },
    { prop: 'emailsocio', name: 'Email Socio' },
    { prop: 'correo_persona', name: 'Email Cliente' },
  ];
  total: number;
  temp: any[] = [];

  constructor(
    private contactosService: ContactosService,
  ) { }


  ngOnInit() {
    this.contactosService.query().subscribe((contactos: any) => {
      this.temp = [...contactos];
      this.rows = contactos;
      this.total = contactos.length;
    });
  }

  ngOnDestroy() {
    this.alive = false;
  }

  onDeleteConfirm(event): void {
    if (window.confirm('¿Seguro que deseas eliminar este usuario?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  filter(event) {
    const val = event.target.value.toLowerCase();

    const temp = this.temp.filter(function(d) {
      return (d.nombre_persona || '').toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.rows = temp;
    this.table.offset = 0;
  }
}