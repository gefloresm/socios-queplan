import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

// Components
import { DatatableComponent } from '@swimlane/ngx-datatable';

// Services
import { SociosService } from '../../@core/data/socios.service';
import { SaldosymovimientosService } from '../../@core/data/saldosymovimientos.service';

@Component({
  selector: 'ngx-saldos-y-movimientos',
  templateUrl: './saldos-y-movimientos.component.html',
  styleUrls: ['./saldos-y-movimientos.component.scss'],
})
export class SaldosYMovimientosComponent implements OnInit, OnDestroy {

  @ViewChild(DatatableComponent) table: DatatableComponent;
  private alive = true;

  formAddBalance: FormGroup;
  formReduceBalance: FormGroup;
  formAddBalanceLoading: boolean = false;
  formReduceBalanceLoading: boolean = false;

  totalSaldo: number = 0;
  socios: any = {
    data: [],
    metadata: 0,
    temp: [],
    loading: true,
  };

  movimientos: any = {
    data: [],
    metadata: 0,
    temp: [],
    loading: true,
  };

  constructor(
    private formBuilder: FormBuilder,
    private _sociosService: SociosService,
    private _saldosymovimientosService: SaldosymovimientosService,
  ) { }

  ngOnInit() {

    this.loadSocios();

    this._saldosymovimientosService.query().subscribe((saldosymovimientos: any) => {
      this.movimientos.temp = [...saldosymovimientos];
      this.movimientos.data = saldosymovimientos;
      this.movimientos.loading = false;
    });

    this.formAddBalance = this.formBuilder.group({
      id_socio: new FormControl('', Validators.required),
      monto: new FormControl('', Validators.required),
      tipo_de_movimiento: new FormControl('', Validators.required),
      descripcion: new FormControl('', Validators.required),
    });

    this.formReduceBalance = this.formBuilder.group({
      id_socio: new FormControl('', Validators.required),
      monto: new FormControl('', Validators.required),
      id_de_solicitud: new FormControl('', Validators.required),
      tipo_de_movimiento: new FormControl('', Validators.required),
      descripcion: new FormControl('', Validators.required),
    });
  }

  ngOnDestroy() {
    this.alive = false;
  }

  loadSocios(): void {
    this._sociosService.query().subscribe((socios: any) => {
      this.socios.temp = [...socios];
      this.socios.data = socios;
      this.totalSaldo = Object.values(socios).reduce((total, item: any) => total + item.saldo, 0);
      this.socios.loading = false;
    });
  }

  onDeleteConfirm(event): void {
    if (window.confirm('¿Seguro que deseas eliminar este usuario?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  filter(event) {
    const val = event.target.value.toLowerCase();

    const temp = this.socios.temp.filter(function (d) {
      return (d.nombre_persona || '').toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.socios.data = temp;
    this.table.offset = 0;
  }

  addBalance() {
    if (this.formAddBalance.valid) {
      this.formAddBalanceLoading = true;
      this._saldosymovimientosService.save(this.formAddBalance.value).subscribe((saldoymovimiento: any) => {
        this.movimientos.data = [saldoymovimiento, ...this.movimientos.data];
        this.formAddBalanceLoading = false;

        this.loadSocios();
      });
    }
  }

  reduceBalance() {
    if (this.formReduceBalance.valid) {
      this.formReduceBalanceLoading = true;
      this._saldosymovimientosService.save(this.formReduceBalance.value).subscribe((saldoymovimiento: any) => {
        this.movimientos.data = [saldoymovimiento, ...this.movimientos.data];
        this.formReduceBalanceLoading = false;

        this.loadSocios();
      });
    }
  }
}
