import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { Subscription } from 'rxjs';

import { AuthService } from '../../../@core/auth/services/auth.service';
import { AnalyticsService } from '../../../@core/utils/analytics.service';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {

  @Input() position = 'normal';

  user: any;
  userMenu = [{ title: 'Perfil' }, { title: 'Cerrar Sesión', link: '/auth/logout' }];
  authSubscribe: Subscription = new Subscription();

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              private authService: AuthService,
              private analyticsService: AnalyticsService,
  ) { }

  ngOnInit() {
    this.authSubscribe = this.authService
      .onCurrentUser()
      .subscribe((user) => this.user = user);
  }

  ngOnDestroy() {
    this.authSubscribe.unsubscribe();
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    return false;
  }

  toggleSettings(): boolean {
    this.sidebarService.toggle(false, 'settings-sidebar');
    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
    this.analyticsService.trackEvent('startSearch');
  }
}
