// List of user roles
export const userRoles = ['guest', 'user', 'admin'];

export default {
    userRoles,
};
