const PROXY_CONFIG =
	{
		"/api": {
		  "target": "http://localhost:3000",
			"secure": false,
			"changeOrigin": true,
			"pathRewrite": {'^/api/auth' : '/auth'},
			"bypass": function (req, res, proxyOptions) {
			}
		}
	}


module.exports = PROXY_CONFIG;
