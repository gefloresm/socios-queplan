'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newSolicitarseguro;

describe('Solicitarseguro API:', function() {
  describe('GET /api/solicitarseguros', function() {
    var solicitarseguros;

    beforeEach(function(done) {
      request(app)
        .get('/api/solicitarseguros')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          solicitarseguros = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(solicitarseguros).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/solicitarseguros', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/solicitarseguros')
        .send({
          name: 'New Solicitarseguro',
          info: 'This is the brand new solicitarseguro!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newSolicitarseguro = res.body;
          done();
        });
    });

    it('should respond with the newly created solicitarseguro', function() {
      expect(newSolicitarseguro.name).to.equal('New Solicitarseguro');
      expect(newSolicitarseguro.info).to.equal('This is the brand new solicitarseguro!!!');
    });
  });

  describe('GET /api/solicitarseguros/:id', function() {
    var solicitarseguro;

    beforeEach(function(done) {
      request(app)
        .get(`/api/solicitarseguros/${newSolicitarseguro._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          solicitarseguro = res.body;
          done();
        });
    });

    afterEach(function() {
      solicitarseguro = {};
    });

    it('should respond with the requested solicitarseguro', function() {
      expect(solicitarseguro.name).to.equal('New Solicitarseguro');
      expect(solicitarseguro.info).to.equal('This is the brand new solicitarseguro!!!');
    });
  });

  describe('PUT /api/solicitarseguros/:id', function() {
    var updatedSolicitarseguro;

    beforeEach(function(done) {
      request(app)
        .put(`/api/solicitarseguros/${newSolicitarseguro._id}`)
        .send({
          name: 'Updated Solicitarseguro',
          info: 'This is the updated solicitarseguro!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedSolicitarseguro = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedSolicitarseguro = {};
    });

    it('should respond with the updated solicitarseguro', function() {
      expect(updatedSolicitarseguro.name).to.equal('Updated Solicitarseguro');
      expect(updatedSolicitarseguro.info).to.equal('This is the updated solicitarseguro!!!');
    });

    it('should respond with the updated solicitarseguro on a subsequent GET', function(done) {
      request(app)
        .get(`/api/solicitarseguros/${newSolicitarseguro._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let solicitarseguro = res.body;

          expect(solicitarseguro.name).to.equal('Updated Solicitarseguro');
          expect(solicitarseguro.info).to.equal('This is the updated solicitarseguro!!!');

          done();
        });
    });
  });

  describe('PATCH /api/solicitarseguros/:id', function() {
    var patchedSolicitarseguro;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/solicitarseguros/${newSolicitarseguro._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Solicitarseguro' },
          { op: 'replace', path: '/info', value: 'This is the patched solicitarseguro!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedSolicitarseguro = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedSolicitarseguro = {};
    });

    it('should respond with the patched solicitarseguro', function() {
      expect(patchedSolicitarseguro.name).to.equal('Patched Solicitarseguro');
      expect(patchedSolicitarseguro.info).to.equal('This is the patched solicitarseguro!!!');
    });
  });

  describe('DELETE /api/solicitarseguros/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/solicitarseguros/${newSolicitarseguro._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when solicitarseguro does not exist', function(done) {
      request(app)
        .delete(`/api/solicitarseguros/${newSolicitarseguro._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
