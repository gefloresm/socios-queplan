/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/solicitarseguros              ->  index
 * POST    /api/solicitarseguros              ->  create
 * GET     /api/solicitarseguros/:id          ->  show
 * PUT     /api/solicitarseguros/:id          ->  upsert
 * PATCH   /api/solicitarseguros/:id          ->  patch
 * DELETE  /api/solicitarseguros/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import { Solicitarseguro, Solicitarhistorico, Administrarsocios } from '../../sqldb';
import seqconsult from '../../sqldb';
import { ipCliente } from '../globale/globale.controller';
import * as queplanback from '@queplan/queplan-back';
import { promises } from 'fs';

//Relations
Administrarsocios.hasMany(Solicitarseguro, { foreignKey: 'asignado_a', as: 'sseg' });
Solicitarseguro.belongsTo(Administrarsocios, { foreignKey: 'asignado_a' });


function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function (entity) {
    try {
      // eslint-disable-next-line prefer-reflect
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch (err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.destroy()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}


// Gets data de Solicitarseguros ordenada por id_solicitud
export function index(req, res) {  //router.get('/', controller.index);
  let paginaSolicitada = req.query.paginaSolicitada;
  let elementosPorPagina = req.query.elementosPorPagina;

  if ((paginaSolicitada || elementosPorPagina) === undefined) {
    return Solicitarseguro.findAll({
      order: 'id_solicitud'
    })
      .then(respondWithResult(res))
      .catch(handleError(res));
  } else {
    elementosPorPagina = Number(elementosPorPagina);
    paginaSolicitada = Number(paginaSolicitada);

    return Solicitarseguro.findAll({
      order: 'id_solicitud',
      offset: (paginaSolicitada * elementosPorPagina),
      limit: elementosPorPagina
    })
      .then(solicitarseguro => {
        Solicitarseguro.count().then(total => {
          res.json({
            data: solicitarseguro,
            total: total
          });
        });
      })
      .catch(handleError(res));
  }
}

// Gets a single Solicitarseguro from the DB for id
export async function show(req, res) {  //router.get('/:id', controller.show);
  let ejecutivos = new Promise((resolve, reject) => {
    Administrarsocios.findAll({
      where: {
        _id: req.params.id
      },
      include: [
        {
          model: Solicitarseguro, as: 'sseg',
          where: {
            asignado_a: req.params.id
          }
          , required: false
        }
      ]
    }).then(response => {
      if (response) {
        resolve(response);
      }
    }).catch(err => {
      reject(err);
    });
  });
  let ejec = await ejecutivos;

  Solicitarseguro.find({
    where: {
      id_solicitud: req.params.id
    }
  }).then(mostrarInfo => {
    res.json({
      data: mostrarInfo,
      ejecutivo: {
        nombres: ejec[0].nombres,
        apellidos: ejec[0].apellidos
      }
    });
  }).catch(handleError(res));
}

// Creates a new Solicitarseguro in the DB
export function create(req, res) {
  return Solicitarseguro.create(req.body)
    .then(respondWithResult(res))
    .catch(handleError('pruebaaa' + res));
}

// Upserts the given Solicitarseguro in the DB at the specified ID (inserta el seguro con un id especifico)
export function upsert(req, res) {
  if (req.body.id_solicitud) {
    Reflect.deleteProperty(req.body, 'id_solicitud');
  }

  return Solicitarseguro.upsert(req.body, {
    where: {
      id_solicitud: req.params.id
    }
  })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Solicitarseguro in the DB --- router.patch('/:id', controller.patch);
export async function patch(req, res) {
  if (req.body.id_solicitud) {
    Reflect.deleteProperty(req.body, 'id_solicitud');
  }

  if (req.body.id_solicitud) {
    delete req.body.id_solicitud;
  }

  //la idea es q se actulice solo los datos que tengo en este json
  let dataupdate = {};
  dataupdate.correo_persona = req.body.correo_persona;
  dataupdate.telefono_persona = req.body.telefono_persona;
  dataupdate.notas_admin = req.body.notas_admin;
  dataupdate.email_notificacion_cliente = req.body.email_notificacion_cliente;


  //dato modificado es el que es diferente a undefined
  let nombreDatoModificado = "";
  let valorModificado = "";
  for (let key in dataupdate) {
    let sal = dataupdate[key];

    if (sal != undefined) {
      nombreDatoModificado = key;
      valorModificado = sal;
    }
  }

  //dato anterior hay que rescatarlo de la bd
  let datoAnterior = "";
  const datoAn = seqconsult.sequelize.query(
    `select ${nombreDatoModificado} from sa.solicitar_seguro where id_solicitud = ${req.params.id}`
  ).then(function (results) {
    for (let key in results) {
      if (key == 0) {
        let www = results[key];
        for (let a in www) {
          let x = www[a];
          datoAnterior = x[nombreDatoModificado]
        }
      }
    }
  });
  await datoAn;

  let dataGuardar = {};
  dataGuardar.id_solicitud = req.params.id;
  dataGuardar.columna_modificada = nombreDatoModificado;
  dataGuardar.dato_anterior = datoAnterior;
  dataGuardar.dato_nuevo = valorModificado;
  dataGuardar.ip_modificacion = ipCliente(req);
  dataGuardar.fecha_modificacion = new Date(Date.now());
  dataGuardar.id_usuario_modificador = req.user.token._id; // req.user.profile.name;

  //insertar data en la tabla de historico  solicitarsegurohistorico
  Solicitarhistorico.create(dataGuardar);
  Solicitarseguro.update(dataupdate, {
    where: {
      id_solicitud: req.params.id
    }
  })
    .then(respondWithResult(res))
    .catch(handleError(res));
}


export function mostrarEstadoSolicitud(req, res) {
  return Solicitarseguro.findAll({ 
    attributes: ['estado_solicitud', [seqconsult.sequelize.fn('count', seqconsult.sequelize.col('estado_solicitud')),'total']],
    group: ['estado_solicitud'],
    order: seqconsult.sequelize.col('estado_solicitud')
    //,raw: true
  }).then(respondWithResult(res))
    .catch(handleError(res));
}

export function mostrarHistoricoSolicitarSeg(req, res) {
  return Solicitarhistorico.findAll({
    where: {
      id_solicitud: req.params.id,
    }
  }).then(respondWithResult(res))
    .catch(handleError(res));
}


// Deletes a Solicitarseguro from the DB
export function destroy(req, res) {
  return Solicitarseguro.find({
    where: {
      id_solicitud: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
