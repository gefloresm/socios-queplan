/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import SolicitarseguroEvents from './solicitar_seguro.events';


// Model events to emit
var events = ['create', 'update','remove'];

export function register(socket) {
  // Bind model events to socket events
  for(var i = 0, eventsLength = events.length; i < eventsLength; i++) {
    var event = events[i];
    var listener = createListener(`solicitar_seguro:${event}`, socket);
    console.log('event: ' + event);
    console.log('listener: ' + listener);
    SolicitarseguroEvents.on(event, listener);
    socket.on('disconnect', removeListener(event, listener));
  }
}

function createListener(event, socket) {
  return function(doc) {
    socket.emit(event, doc);
  };
}

function removeListener(event, listener) {
  return function() {
    SolicitarseguroEvents.removeListener(event, listener);
  };
}
