'use strict';

export default function(sequelize, DataTypes) {
  return sequelize.define('Solicitarplan', {
    _id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    nombrepersona: DataTypes.STRING,
    rutpersona: DataTypes.STRING,
    correopersona: DataTypes.STRING,
    telefonopersona: DataTypes.STRING,
    nombreplan: DataTypes.STRING,
    codigoisapre: DataTypes.INTEGER,
    datospersona: DataTypes.JSON,
    desde: DataTypes.STRING,
    ip: DataTypes.STRING,
    fecha: {type: DataTypes.DATE, defaultValue: DataTypes.NOW},
    emailsocio: DataTypes.TEXT,
    emailcliente: DataTypes.TEXT,
    email_misma_isapre: DataTypes.BOOLEAN,
    asignado_a: DataTypes.INTEGER,
    estado_solicitud: DataTypes.INTEGER,
    notas_socio: DataTypes.TEXT,
    notas_admin: DataTypes.TEXT,
    fecha_asignado: DataTypes.DATE,
    ultimo_estado_socio: DataTypes.DATE,
    ultimo_estado_admin: DataTypes.DATE,
    precio_solicitado: DataTypes.REAL,
    es_de_la_misma_isapre: DataTypes.BOOLEAN,
    razon_fallo: DataTypes.INTEGER,
    nombre_largo_plan: DataTypes.STRING,
    asignado_por: DataTypes.INTEGER,
    region: DataTypes.JSON,
    tipo: DataTypes.STRING,
    aseguradora_actual: DataTypes.INTEGER
  });
}
