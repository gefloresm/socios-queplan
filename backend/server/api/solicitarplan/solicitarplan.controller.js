/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/solicitarplans              ->  index
 * POST    /api/solicitarplans              ->  create
 * GET     /api/solicitarplans/:id          ->  show
 * PUT     /api/solicitarplans/:id          ->  upsert
 * PATCH   /api/solicitarplans/:id          ->  patch
 * DELETE  /api/solicitarplans/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import { Solicitarplan, Administrarsocios, Solicitarhistorico } from '../../sqldb';
import { ipCliente } from '../globale/globale.controller';

import seqconsult from '../../sqldb';
import nodemailer from 'nodemailer';
import hbs from 'nodemailer-express-handlebars';


//Información de las Isapres

var infoisapres = [{
    codigodelaisapre: 62,
    nombreisapre: "San Lorenzo Ltda.",
    imagenisapre: "assets/images/isapres/Logo_Isapre_San_Lorenzo.jpg",
    abvisapre: "San Lorenzo",
    urlisapre: "Isapre-San-Lorenzo",
    abierta: false
}, {
    codigodelaisapre: 63,
    nombreisapre: "Fusat Ltda.",
    imagenisapre: "assets/images/isapres/fusat.jpg",
    abvisapre: "Fusat",
    urlisapre: "Isapre-Fusat",
    abierta: false
}, {
    codigodelaisapre: 65,
    nombreisapre: "Chuquicamata Ltda.",
    abvisapre: "Chuquicamata",
    imagenisapre: "assets/images/isapres/Logo_Isapre_Chuquicamata.jpg",
    urlisapre: "Isapre-Chuquicamata",
    abierta: false
}, {
    codigodelaisapre: 67,
    nombreisapre: "Colmena Golden Cross",
    imagenisapre: "assets/images/isapres/colmena.png",
    abvisapre: "Colmena",
    preciogesl: 0.52,
    urlisapre: "Isapre-Colmena",
    abierta: true
}, {
    codigodelaisapre: 68,
    nombreisapre: "Río Blanco Ltda.",
    imagenisapre: "assets/images/isapres/rioblanco.jpg",
    abvisapre: "Río Blanco",
    urlisapre: "Isapre-Rio-Blanco",
    abierta: false
}, {
    codigodelaisapre: 76,
    nombreisapre: "Fundación Ltda.",
    abvisapre: "Isapre Fundación",
    imagenisapre: "assets/images/isapres/fundacion.png",
    urlisapre: "Isapre-Fundacion",
    abierta: false
}, {
    codigodelaisapre: 78,
    nombreisapre: "CruzBlanca",
    imagenisapre: "assets/images/isapres/cruzblanca.png",
    abvisapre: "CruzBlanca",
    preciogesl: 0.513,
    urlisapre: "Isapre-CruzBlanca",
    abierta: true
}, {
    codigodelaisapre: 80,
    nombreisapre: "Vida Tres",
    imagenisapre: "assets/images/isapres/vidatres.png",
    abvisapre: "Vida Tres",
    preciogesl: 0.39,
    urlisapre: "Isapre-Vida-Tres",
    abierta: true
}, {
    codigodelaisapre: 81,
    nombreisapre: "Nueva Masvida",
    imagenisapre: "assets/images/isapres/masvida.png",
    abvisapre: "Masvida",
    preciogesl: 0.27,
    urlisapre: "Isapre-Nueva-Masvida",
    abierta: true
}, {
    codigodelaisapre: 88,
    nombreisapre: "Nueva Masvida",
    imagenisapre: "assets/images/isapres/masvida.png",
    abvisapre: "Nueva Masvida",
    preciogesl: 0.45,
    urlisapre: "Isapre-Nueva-Masvida",
    abierta: true
}, {
    codigodelaisapre: 99,
    nombreisapre: "Banmédica",
    imagenisapre: "assets/images/isapres/banmedica.png",
    abvisapre: "Banmédica",
    preciogesl: 0.36,
    urlisapre: "Isapre-Banmedica",
    abierta: true
}, {
    codigodelaisapre: 107,
    nombreisapre: "Consalud",
    imagenisapre: "assets/images/isapres/consalud.png",
    abvisapre: "Consalud",
    preciogesl: 0.44,
    urlisapre: "Isapre-Consalud",
    abierta: true
}, {
    codigodelaisapre: 94,
    nombreisapre: "Cruz del Norte Ltda.",
    imagenisapre: "assets/images/isapres/cruzdelnorte.png",
    abvisapre: "Cruz del Norte",
    urlisapre: "Isapre-Cruz-del-Norte",
    abierta: false
}, {
    codigodelaisapre: 999,
    nombreisapre: "Resto de las isapres",
    abvisapre: "Otras",
    imagenisapre: null,
    abierta: false
}];

// Correo de envío con datos del socio

//Variable GLobal para identificar donde actualizar
var idaactualizar = 1;

// Se usa helper para tener un index + 1 en el archivo hbs socios
var options = {
    viewEngine: {
        extname: '.hbs',
        layoutsDir: 'server/views/email/', // Revisar ruta
        helpers: {
            inc: function (value, options) {
                return parseInt(value) + 1;
            }
        }
    },
    viewPath: 'server/views/email/', // Revisar ruta
    extName: '.hbs'
};

export function asignarSocioEmail(req, res) {
    var datossocio;
    var datossolicitud;
    return Administrarsocios.find({
        where: {
            _id: req.body.id_socio
        }
    })
        .then(handleEntityNotFound(res))
        .then(function (entity) {
            if (entity) {
                datossocio = entity;
                return Solicitarplan.find({
                    where: {
                        _id: req.body.id_solicitud
                    }
                })
            }
        })
        .then(function (entity) {
            datossolicitud = entity;
            /*                    console.log(datossolicitud);
                                console.log("Datos socio");
                                console.log(datossocio.nombres);
                                console.log(datossocio.telefono_1);
                                console.log(datossocio.email_personal);
                                console.log(datossocio.isapre);
                                console.log("Datos Persona");
                                console.log(datossolicitud.nombrepersona);*/
            enviarMailAsignacion(req, res, datossocio, datossolicitud);
        })
        .catch(handleError(res));
}

export function enviarEmailaSocio(req, res) {
    var datossocio;
    var datossolicitud;
    return Administrarsocios.find({
        where: {
            _id: req.body.id_socio
        }
    })
        .then(handleEntityNotFound(res))
        .then(function (entity) {
            if (entity) {
                datossocio = entity;
                return Solicitarplan.find({
                    where: {
                        _id: req.body.id_solicitud
                    }
                })
            }
        })
        .then(function (entity) {
            datossolicitud = entity;
            enviarEmailaSocio(req, res, datossocio, datossolicitud);
        })
        .then(function (entity) {
            // Despues de haber asigando se suma 
            let contadorAsignado = parseInt(datossocio.getAsignaciones.asignados_hoy) + 1;
            let socio_id = parseInt(datossocio.getAsignaciones.id);
            let codigoIsapre = parseInt(datossocio.getAsignaciones.isapre);
            let region_numero = parseInt(datossocio.getAsignaciones.region_principal);

            Administrarsocios.update(
                { asignados_hoy: contadorAsignado },
                { where: { _id: socio_id } }
            ).then(() => {
                seqconsult.sequelize.query(
                    `UPDATE "Administrarsocios"
                    SET sobrecupo = sobrecupo + CAST((SELECT option_value FROM constantes.configuraciones_globales WHERE option_id = 4) AS integer)
                    WHERE isapre = :codigoisapre AND (
                        SELECT bool_and(asignados_hoy >= ( CAST((SELECT option_value FROM constantes.configuraciones_globales WHERE option_id = 3) AS integer) + sobrecupo )) 
                        FROM "Administrarsocios" 
                        WHERE isapre = :codigoisapre AND 
                        estado = TRUE AND 
                        tmp_suspendido = FALSE AND 
                        (region_principal = :region_numero OR region_secundaria = :region_numero) 
                        ) = TRUE AND (region_principal = :region_numero OR region_secundaria = :region_numero)
                        `, {
                        replacements: {
                            codigoisapre: codigoIsapre,
                            region_numero: region_numero
                        }, type: seqconsult.sequelize.QueryTypes.SELECT
                    })
            })
        })
        .catch(handleError(res));
}

// Email a Cliente
var enviarMailAsignacion = function (req, res, datossocio, datossolicitud) {
    console.log("Email de asignación");

    var noperaciones = 1;
    var respuestaemailcliente = 'Sin respuesta 2';

    //Despues de ejecutar las 3 operaciones se registra si se envio el email
    var operaciondespues = function () {
        console.log("Se cumplieron pasos con el id a actualizar:")
        console.log(idaactualizar);

        /*        Solicitarplan.update({
                        emailcliente: respuestaemailcliente,
                        emailsocio: respuestaemailcliente
                    }, {
                        fields: ['emailcliente', 'emailsocio'],
                        where: {
                            _id: idaactualizar
                        }
                    })
                    .then(respondWithResulttres(res, 200))
                    .catch(handleErrorsinresp(res));*/
    }
    // Registra si se van cumpliendo
    var secumplorero = function () {
        noperaciones--;
        console.log("Conteo de operaciones restantes " + noperaciones);
        if (noperaciones == 0) {
            operaciondespues();
        }
    }


    //Se define el registro a guardar
    var guardarregistro = {};
    guardarregistro.id_solicitud = datossocio;
    guardarregistro.socio = datossolicitud;
    // Ip Cliente 
    guardarregistro.ip = (req.headers['cf-connecting-ip'] ||
        req.headers['x-forwarded-for'] ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        req.connection.socket.remoteAddress).split(",")[0];
    guardarregistro.emailcliente = 'Sin respuesta';

    console.log(guardarregistro);


    var transporter = nodemailer.createTransport({
        host: 'smtp.zoho.com',
        port: 465,
        secure: true, //true --> will use ssl
        auth: {
            user: 'contacto@queplan.cl',
            pass: 'iqclp43s'
        }
    });


    transporter.use('compile', hbs(options));

    // Datos de la Isapre desde el Array
    var isapreencontrada = infoisapres.filter(function (obj) {
        var result = obj.codigodelaisapre == datossolicitud.codigoisapre;
        return result;
    })[0];

    var mailOptionscliente = {
        from: 'contacto@queplan.cl', // sender address
        to: datossolicitud.correopersona,
        template: 'asignarejecutivo',
        //  to: 'solicitudes.queplan@gmail.com ,rkerrc@outlook.com', // list of receivers
        subject: 'Tu solicitud ha sido asignada, Isapre ' + isapreencontrada.nombreisapre,
        context: {
            datossocio: datossocio,
            datossolicitud: datossolicitud,
            isapreencontrada: isapreencontrada
        }
    };


    transporter.sendMail(mailOptionscliente, function (error, info) {
        if (error) {
            console.log(error);
            respuestaemailcliente = JSON.stringify(error);
            secumplorero();
            res.status(500).json(respuestaemailcliente);
        } else {
            console.log('Email cliente enviado: ' + info.response);
            respuestaemailcliente = 'Email cliente enviado: ' + info.response;
            secumplorero();
            res.status(200).json(respuestaemailcliente);
        };
    });

    //Fin Email
    return null;
}

// Email a Socio
var enviarEmailaSocio = function (req, res, datossocio, datossolicitud) {
    console.log("Email a Socio");

    console.log("\n -------------------------------------------- \n");

    /*     // Despues de haber asigando se resta 
        let contadorAsignado = parseInt(datossocio.getAsignaciones.asignados_hoy) + 1;
        let socio_id = parseInt(datossocio.getAsignaciones.id);
        let codigoIsapre = parseInt(datossocio.getAsignaciones.isapre);
        console.log(contadorAsignado);
        console.log(socio_id);
        console.log(codigoIsapre);
        console.log("Ejecutar UPDATE");
        AdministrarSocios.update(
          {asignados_hoy: contadorAsignado},
          {where: {_id: socio_id} }
        ).then(result => {
            console.log("Ejecutar Raw query");
            seqconsult.sequelize.query(
                `UPDATE "Administrarsocios"
                    SET sobrecupo = sobrecupo + CAST((SELECT option_value FROM constantes.configuraciones_globales WHERE option_id = 4) AS integer)
                WHERE isapre = :codigoisapre AND ((SELECT bool_and(asignados_hoy >= ((
                    SELECT sum(cast(option_value AS integer)) FROM constantes.configuraciones_globales WHERE option_id IN (3,4)) )) 
                    FROM "Administrarsocios" WHERE isapre = :codigoisapre)) = TRUE`, {
                replacements: {
                    codigoisapre: codigoIsapre
                }, type: seqconsult.sequelize.QueryTypes.SELECT
            })
        })
     */
    var noperaciones = 1;
    var respuestaemailcliente = 'Sin respuesta 2';

    //Despues de ejecutar las 3 operaciones se registra si se envio el email
    var operaciondespues = function () {
        console.log("Se cumplieron pasos con el id a actualizar:")
        console.log(idaactualizar);

        /*        Solicitarplan.update({
                        emailcliente: respuestaemailcliente,
                        emailsocio: respuestaemailcliente
                    }, {
                        fields: ['emailcliente', 'emailsocio'],
                        where: {
                            _id: idaactualizar
                        }
                    })
                    .then(respondWithResulttres(res, 200))
                    .catch(handleErrorsinresp(res));*/
    }
    // Registra si se van cumpliendo
    var secumplorero = function () {
        noperaciones--;
        console.log("Conteo de operaciones restantes " + noperaciones);
        if (noperaciones == 0) {
            operaciondespues();
        }
    }


    //Se define el registro a guardar
    var guardarregistro = {};
    guardarregistro.id_solicitud = datossocio;
    guardarregistro.socio = datossolicitud;
    // Ip Cliente 
    guardarregistro.ip = (req.headers['cf-connecting-ip'] ||
        req.headers['x-forwarded-for'] ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        req.connection.socket.remoteAddress).split(",")[0];
    guardarregistro.emailcliente = 'Sin respuesta';

    console.log(guardarregistro);

    // Comprueba si hay un segundo cotizante y lo imprime
    var segundocotizante = false;
    if (datossolicitud.datospersona.cotizante[1]) {
        segundocotizante = true;
    } else { }


    var transporter = nodemailer.createTransport({
        host: 'smtp.zoho.com',
        port: 465,
        secure: true, //true --> will use ssl
        auth: {
            user: 'contacto@queplan.cl',
            pass: 'iqclp43s'
        }
    });


    transporter.use('compile', hbs(options));

    // Datos de la Isapre desde el Array
    var isapreencontrada = infoisapres.filter(function (obj) {
        var result = obj.codigodelaisapre == datossolicitud.codigoisapre;
        return result;
    })[0];

    var mailOptionscliente = {
        from: 'contacto@queplan.cl', // sender address
        to: datossocio.email_personal,
        template: 'emailaejecutivo',
        //  to: 'solicitudes.queplan@gmail.com ,rkerrc@outlook.com', // list of receivers
        subject: '¡Se te ha asignado un potencial cliente y está a la espera de tu llamado! ' + isapreencontrada.nombreisapre,
        context: {
            datossocio: datossocio,
            datossolicitud: datossolicitud,
            isapreencontrada: isapreencontrada,
            segundocotizante: segundocotizante
        }
    };


    transporter.sendMail(mailOptionscliente, function (error, info) {
        if (error) {
            console.log(error);
            respuestaemailcliente = JSON.stringify(error);
            secumplorero();
            res.status(500).json(respuestaemailcliente);
        } else {
            console.log('Email Socio enviado: ' + info.response);
            respuestaemailcliente = 'Email Socio enviado: ' + info.response;
            secumplorero();
            res.status(200).json(respuestaemailcliente);
        };
    });

    //Fin Email
    return null;
}


function respondWithResult(res, statusCode) {
    statusCode = statusCode || 200;
    return function (entity) {
        if (entity) {
            return res.status(statusCode).json(entity);
        }
        return null;
    };
}

function patchUpdates(patches) {
    return function (entity) {
        try {
            jsonpatch.apply(entity, patches, /*validate*/ true);
        } catch (err) {
            return Promise.reject(err);
        }

        return entity.save();
    };
}

function removeEntity(res) {
    return function (entity) {
        if (entity) {
            return entity.destroy()
                .then(() => {
                    res.status(204).end();
                });
        }
    };
}

function handleEntityNotFound(res) {
    return function (entity) {
        if (!entity) {
            res.status(404).end();
            return null;
        }
        return entity;
    };
}

function handleError(res, statusCode) {
    statusCode = statusCode || 500;
    return function (err) {
        res.status(statusCode).send(err);
    };
}

// Gets a list of Solicitarplans
export function index(req, res) {
    var ipcliente = (req.headers['cf-connecting-ip'] ||
        req.headers['x-forwarded-for'] ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        req.connection.socket.remoteAddress).split(",")[0];

    let condicion = {};
    let columnas = ['_id', 'nombrepersona', 'rutpersona', 'correopersona', 'telefonopersona', 'nombreplan', 'codigoisapre', 'datospersona', 'fecha', 'emailsocio', 'emailcliente', 'asignado_a', 'estado_solicitud', 'notas_admin', 'ultimo_estado_admin', 'precio_solicitado', 'razon_fallo', 'fecha_asignado', 'nombre_largo_plan', 'asignado_por', 'region'];

    console.log("el siguiente usuario llegó a listado solicitudes: " + req.user._id + " con el siguiente role: " + req.user.role + ' ip: ' + ipcliente);
    // No usadas aún
    // 'desde', 'email_asignacion_cliente', 'email_asignacion_ejecutivo'
    if (req.user.role === 'cb') {
        condicion = {
            // atenea1508cb
            // queplanzeus78
            // artemisa78qp
            codigoisapre: 78,
            _id: { $gte: 2498 }
        }
    } else if (req.user.role === 'consalud') {
        condicion = {
            codigoisapre: 107,
            _id: { $gte: 12983 },
            region: {
                $and: { numero: 13 }
            },
            asignado_a: { $notIn: [122, 177] }
        }
    } else if (req.user.role === 'admin') {
        condicion = {
            _id: { $gte: 1600 }
        };
        columnas.push('es_de_la_misma_isapre', 'email_misma_isapre', 'aseguradora_actual', 'tipo');
    } else {
        condicion = { codigoisapre: 0 }
    }
    return Solicitarplan.findAll({
        attributes: columnas,
        where: condicion,
        order: [['_id', 'DESC']]
    })
        .then(respondWithResult(res))
        .catch(handleError(res));
}

// Gets a single Solicitarplan from the DB
export function solicitudesdelsocio(req, res) {

    var ipcliente = (req.headers['cf-connecting-ip'] ||
        req.headers['x-forwarded-for'] ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        req.connection.socket.remoteAddress).split(",")[0];

    seqconsult.sequelize.query(
        `SELECT COALESCE((SELECT duplicado FROM asignaciones_automatomaticas_log WHERE solicitud_id = s."_id" LIMIT 1), FALSE) AS duplicado_busqueda, * FROM "Solicitarplans" AS s
        WHERE s.asignado_a = (SELECT _id FROM "Administrarsocios" WHERE id_de_usuario = ? LIMIT 1)
        and estado_solicitud in (2,6)
        ORDER BY _id DESC;` ,
        { replacements: [req.user._id], type: seqconsult.sequelize.QueryTypes.SELECT }
    ).then(function (results) {
        console.log("el siguiente usuario llegó a solicitudes del soc: " + req.user._id + ' ip: ' + ipcliente);
        res.json(results);
    })
}

// Creates a new Solicitarplan in the DB
export function create(req, res) {
    return Solicitarplan.create(req.body)
        .then(respondWithResult(res, 201))
        .catch(handleError(res));
}

// Upserts the given Solicitarplan in the DB at the specified ID
export function upsert(req, res) {
    if (req.body._id) {
        delete req.body._id;
    }

    return Solicitarplan.upsert(req.body, {
        where: {
            _id: req.params.id
        }
    })
        .then(respondWithResult(res))
        .catch(handleError(res));
}

// Updates an existing Solicitarplan in the DB
export async function patch(req, res) {
    console.log("Imprimir Body");
    console.log(req.body);
    console.log("Imprimir Parametro");
    console.log(req.params);
    console.log("Imprimir Req");
    console.log(req.user.role);
    if (req.body._id) {
        delete req.body._id;
    }
    var actualizar;
    actualizar = {};
    actualizar.asignado_a = req.body.asignado_a;
    actualizar.estado_solicitud = req.body.estado_solicitud;
    actualizar.notas_admin = req.body.notas_admin;
    actualizar.razon_fallo = req.body.razon_fallo;

    //dato modificado es el que es diferente a undefined
    let nombreDatoModificado = "";
    let valorModificado = "";
    for (let key in actualizar) {
        let sal = actualizar[key];

        if (sal != undefined) {
            nombreDatoModificado = key;
            valorModificado = sal;
        }
    }
    console.log("nombreDatoModificado");
    console.log(nombreDatoModificado);

    console.log("valorModificado");
    console.log(valorModificado);

    if (req.body.asignado_por === null) {
        actualizar.asignado_por = req.user._id;
    }

    console.log(req.body.cambiosenasignarejec);
    if (req.body.cambiosenasignarejec || typeof req.body.cambiosenasignarejec === 'undefined' || typeof req.body.cambiosenasignarejec === 'null') {
        actualizar.fecha_asignado = new Date(Date.now());
    }

    //dato anterior hay que rescatarlo de la bd
    let datoAnterior = "";
    const datoAn = seqconsult.sequelize.query(
        `select ${nombreDatoModificado} from public."Solicitarplans" where _id = ${req.params.id}`
    ).then(function (results) {
        for (let key in results) {
            if (key == 0) {
                let www = results[key];
                for (let a in www) {
                    let x = www[a];
                    datoAnterior = x[nombreDatoModificado]
                }
            }
        }
    });
    await datoAn;

    console.log("datoAnterior");
    console.log(datoAnterior);
    let dataGuardar = {};
    dataGuardar.id_solicitud = req.params.id;
    dataGuardar.columna_modificada = nombreDatoModificado;
    dataGuardar.dato_anterior = datoAnterior;
    dataGuardar.dato_nuevo = valorModificado;
    dataGuardar.ip_modificacion = ipCliente(req);
    dataGuardar.fecha_modificacion = new Date(Date.now());
    dataGuardar.id_usuario_modificador = req.user.token._id; // req.user.profile.name;

    //insertar data en la tabla de historico  solicitarsegurohistorico
    Solicitarhistorico.create(dataGuardar);

    return Solicitarplan.update(actualizar, {
        where: {
            _id: req.params.id
        }
    })
        .then(respondWithResult(res))
        .catch(handleError(res));
}



// Deletes a Solicitarplan from the DB
export function destroy(req, res) {
    return Solicitarplan.find({
        where: {
            _id: req.params.id
        }
    })
        .then(handleEntityNotFound(res))
        .then(removeEntity(res))
        .catch(handleError(res));
}
