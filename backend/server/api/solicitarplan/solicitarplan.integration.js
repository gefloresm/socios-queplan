'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newSolicitarplan;

describe('Solicitarplan API:', function() {
  describe('GET /api/solicitarplans', function() {
    var solicitarplans;

    beforeEach(function(done) {
      request(app)
        .get('/api/solicitarplans')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          solicitarplans = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(solicitarplans).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/solicitarplans', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/solicitarplans')
        .send({
          name: 'New Solicitarplan',
          info: 'This is the brand new solicitarplan!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newSolicitarplan = res.body;
          done();
        });
    });

    it('should respond with the newly created solicitarplan', function() {
      expect(newSolicitarplan.name).to.equal('New Solicitarplan');
      expect(newSolicitarplan.info).to.equal('This is the brand new solicitarplan!!!');
    });
  });

  describe('GET /api/solicitarplans/:id', function() {
    var solicitarplan;

    beforeEach(function(done) {
      request(app)
        .get(`/api/solicitarplans/${newSolicitarplan._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          solicitarplan = res.body;
          done();
        });
    });

    afterEach(function() {
      solicitarplan = {};
    });

    it('should respond with the requested solicitarplan', function() {
      expect(solicitarplan.name).to.equal('New Solicitarplan');
      expect(solicitarplan.info).to.equal('This is the brand new solicitarplan!!!');
    });
  });

  describe('PUT /api/solicitarplans/:id', function() {
    var updatedSolicitarplan;

    beforeEach(function(done) {
      request(app)
        .put(`/api/solicitarplans/${newSolicitarplan._id}`)
        .send({
          name: 'Updated Solicitarplan',
          info: 'This is the updated solicitarplan!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedSolicitarplan = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedSolicitarplan = {};
    });

    it('should respond with the updated solicitarplan', function() {
      expect(updatedSolicitarplan.name).to.equal('Updated Solicitarplan');
      expect(updatedSolicitarplan.info).to.equal('This is the updated solicitarplan!!!');
    });

    it('should respond with the updated solicitarplan on a subsequent GET', function(done) {
      request(app)
        .get(`/api/solicitarplans/${newSolicitarplan._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let solicitarplan = res.body;

          expect(solicitarplan.name).to.equal('Updated Solicitarplan');
          expect(solicitarplan.info).to.equal('This is the updated solicitarplan!!!');

          done();
        });
    });
  });

  describe('PATCH /api/solicitarplans/:id', function() {
    var patchedSolicitarplan;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/solicitarplans/${newSolicitarplan._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Solicitarplan' },
          { op: 'replace', path: '/info', value: 'This is the patched solicitarplan!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedSolicitarplan = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedSolicitarplan = {};
    });

    it('should respond with the patched solicitarplan', function() {
      expect(patchedSolicitarplan.name).to.equal('Patched Solicitarplan');
      expect(patchedSolicitarplan.info).to.equal('This is the patched solicitarplan!!!');
    });
  });

  describe('DELETE /api/solicitarplans/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/solicitarplans/${newSolicitarplan._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when solicitarplan does not exist', function(done) {
      request(app)
        .delete(`/api/solicitarplans/${newSolicitarplan._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
