'use strict';

import * as queplanback from '@queplan/queplan-back';
import seqconsult from '../../sqldb';

export function bajoSaldo(req, res) {
    let id_socio = req.body.id_socio;
    let filtro = [
        { "field": "tag", "key": "id_socio", "relation": "=", "value": id_socio }
    ];
    let mensaje = "Saldo bajo los 80.000";
    let segmento = [];
    console.log(`Ejecutando api/notificaciones/bajo-saldo\nreq.body: ${JSON.stringify(req.body)}\nFiltro: ${JSON.stringify(filtro)}\nSegmento: ${JSON.stringify(segmento)}\nMensaje: ${mensaje}`);
    queplanback.oneSignal.sendNotification(mensaje, segmento, filtro)
        .then((result) => {
            console.log(`Ejecucion exitosa de registra usuario.\n Resultado: ${JSON.stringify(result)}`);
            res.json({"estado": "ok"})
        })
        .catch(err => {
            console.error({"origen": "notificacion", "function": "bajoSaldo", "err": String(err)});
            res.json({"estado": "error", "mensaje": "Debe especificar al menos un segmento o filtro"});
        });
}

export function registraUsuario(req, res) {
    return seqconsult.sequelize.query(
        `SELECT a."_id" AS id_socio, a.isapre FROM "Administrarsocios" AS a 
         INNER JOIN "Users" AS u ON a.id_de_usuario = u."_id" AND u._id = :id_usuario`, {
        replacements: {
            id_usuario: req.user._id
        }, type: seqconsult.sequelize.QueryTypes.SELECT
    }).then(result => {
        if (result.length == 1) {
            let playerId = '';
            if (req.playerId) {
                playerId = req.playerId;
            } else {
                playerId = req.body.playerId;
            }
            let datos = result[0];
            let id_socio = datos.id_socio;
            let isapre = datos.isapre;
            let id_device = req.id_device;
            let tags = {
                'id_socio': id_socio,
                'isapre': isapre,
                'id_device': id_device
            };
            console.log(`Ejecutando api/notificaciones/registrar\nplayerId: ${playerId}\nDatos: ${JSON.stringify(datos)}\nid_socio: ${id_socio}\nisapre: ${isapre}\nid_device:${id_device}\ntags: ${JSON.stringify(tags)}`);
            return queplanback.oneSignal.editDevice(playerId, tags)
                .then(result => {
                    console.log(`Ejecucion exitosa de registra usuario.\n Resultado: ${JSON.stringify(result)}`);
                    res.json({"estado": "ok"});
                })
                .catch(err => {
                    console.error({"origen": "notificacion", "function": "registraUsuario", "err": String(err)});
                    res.json({"estado": "err", "mensaje": String(err)});
                })
        } else {
            res.json({"estado": "error", "mensaje": "Usuario no se encuentra registrador como Socio"});
        }
    });
}

export function prospectoNuevo(req, res) {
    let id_socio = req.body.id_socio;
    let filtro = [
        { "field": "tag", "key": "id_socio", "relation": "=", "value": id_socio }
    ];
    let segmento = [];
    let mensaje = "¡Se te ha asignado un prospecto que espera tu llamado! No dejes que se enfríe";
    console.log(`Ejecutando api/notificaciones/prospecto-nuevo\nreq.body: ${req.body}\nFiltro: ${filtro}\nSegmento: ${segmento}\nMensaje: ${mensaje}`);
    return queplanback.oneSignal.sendNotification(mensaje, segmento, filtro)
        .then(result => {
            console.log(`Ejecucion exitosa de notificacion.\n Resultado: ${JSON.stringify(result)}`);
            res.json({"estado": "ok"})
        })
        .catch(err => {
            console.error({"origen": "notificacion", "function": "prospectoNuevo", "err": String(err)});
            res.json({"estado": "error", "mensaje": "Debe especificar al menos un segmento o filtro"});
        });
}