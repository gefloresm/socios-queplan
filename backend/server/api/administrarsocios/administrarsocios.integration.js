'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newAdministrarsocios;

describe('Administrarsocios API:', function() {
  describe('GET /api/administrarsocioss', function() {
    var administrarsocioss;

    beforeEach(function(done) {
      request(app)
        .get('/api/administrarsocioss')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          administrarsocioss = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(administrarsocioss).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/administrarsocioss', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/administrarsocioss')
        .send({
          name: 'New Administrarsocios',
          info: 'This is the brand new administrarsocios!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newAdministrarsocios = res.body;
          done();
        });
    });

    it('should respond with the newly created administrarsocios', function() {
      expect(newAdministrarsocios.name).to.equal('New Administrarsocios');
      expect(newAdministrarsocios.info).to.equal('This is the brand new administrarsocios!!!');
    });
  });

  describe('GET /api/administrarsocioss/:id', function() {
    var administrarsocios;

    beforeEach(function(done) {
      request(app)
        .get(`/api/administrarsocioss/${newAdministrarsocios._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          administrarsocios = res.body;
          done();
        });
    });

    afterEach(function() {
      administrarsocios = {};
    });

    it('should respond with the requested administrarsocios', function() {
      expect(administrarsocios.name).to.equal('New Administrarsocios');
      expect(administrarsocios.info).to.equal('This is the brand new administrarsocios!!!');
    });
  });

  describe('PUT /api/administrarsocioss/:id', function() {
    var updatedAdministrarsocios;

    beforeEach(function(done) {
      request(app)
        .put(`/api/administrarsocioss/${newAdministrarsocios._id}`)
        .send({
          name: 'Updated Administrarsocios',
          info: 'This is the updated administrarsocios!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedAdministrarsocios = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedAdministrarsocios = {};
    });

    it('should respond with the updated administrarsocios', function() {
      expect(updatedAdministrarsocios.name).to.equal('Updated Administrarsocios');
      expect(updatedAdministrarsocios.info).to.equal('This is the updated administrarsocios!!!');
    });

    it('should respond with the updated administrarsocios on a subsequent GET', function(done) {
      request(app)
        .get(`/api/administrarsocioss/${newAdministrarsocios._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let administrarsocios = res.body;

          expect(administrarsocios.name).to.equal('Updated Administrarsocios');
          expect(administrarsocios.info).to.equal('This is the updated administrarsocios!!!');

          done();
        });
    });
  });

  describe('PATCH /api/administrarsocioss/:id', function() {
    var patchedAdministrarsocios;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/administrarsocioss/${newAdministrarsocios._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Administrarsocios' },
          { op: 'replace', path: '/info', value: 'This is the patched administrarsocios!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedAdministrarsocios = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedAdministrarsocios = {};
    });

    it('should respond with the patched administrarsocios', function() {
      expect(patchedAdministrarsocios.name).to.equal('Patched Administrarsocios');
      expect(patchedAdministrarsocios.info).to.equal('This is the patched administrarsocios!!!');
    });
  });

  describe('DELETE /api/administrarsocioss/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/administrarsocioss/${newAdministrarsocios._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when administrarsocios does not exist', function(done) {
      request(app)
        .delete(`/api/administrarsocioss/${newAdministrarsocios._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
