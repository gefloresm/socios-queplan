'use strict';

export default function(sequelize, DataTypes) {
  return sequelize.define('Administrarsocios', {
    _id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    nombres: DataTypes.STRING,
    apellidos: DataTypes.STRING,
    rut_socio: DataTypes.STRING,
    isapre: DataTypes.INTEGER,
    estado: DataTypes.BOOLEAN,
    registro_en_bd: {type: DataTypes.DATE, defaultValue: DataTypes.NOW},
    telefono_1: DataTypes.STRING,
    telefono_2:DataTypes.STRING,
    email_corporativo: DataTypes.STRING,
    email_personal: DataTypes.STRING,
    correo_preferencia: DataTypes.STRING,
    ciudad: DataTypes.STRING,
    region_principal: DataTypes.INTEGER,
    region_secundaria: DataTypes.INTEGER,
    direccion_personal: DataTypes.STRING,
    saldo: {type: DataTypes.INTEGER,
    defaultValue: 0},
    id_de_usuario: DataTypes.INTEGER,
    tarifa: DataTypes.INTEGER,
    tarifa_fija_por_envio: DataTypes.INTEGER,
    tmp_suspendido: DataTypes.BOOLEAN,
    asignados_hoy: DataTypes.INTEGER,
    sobrecupo: DataTypes.INTEGER,
    especial_alta_uf: DataTypes.BOOLEAN
  }, {
    schema: 'public',
    tableName: "Administrarsocios",
    getterMethods: {
      getAsignaciones() {
        return {
          id: this._id,
          isapre: this.isapre,
          estado: this.estado,
          tmp_suspendido: this.tmp_suspendido,
          asignados_hoy: this.asignados_hoy,
          sobrecupo: this.sobrecupo,
          region_principal: this.region_principal
        }
      },
      getId() {
        return {
          id: this._id,
          isapre: this.isapre
        }
      }
    }
  });
}
