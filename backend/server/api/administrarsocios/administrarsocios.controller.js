/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/administrarsocioss              ->  index
 * POST    /api/administrarsocioss              ->  create
 * GET     /api/administrarsocioss/:id          ->  show
 * PUT     /api/administrarsocioss/:id          ->  upsert
 * PATCH   /api/administrarsocioss/:id          ->  patch
 * DELETE  /api/administrarsocioss/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import { Administrarsocios, User } from '../../sqldb';
import seqconsult from '../../sqldb';

function verificarAcceso(req) {
  if (req.user.role === 'cb') {
    return {
      isapre: 78,
      _id: { $gte: 39 }
    };
  } else if (req.user.role === 'consalud') {
    return {
      isapre: 107,
      _id: { $gte: 188 }
    };
  } else if (req.user.role === 'admin') {
    return {};
  }
  return { isapre: 0 };
}

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function (entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch (err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.destroy()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Administrarsocioss
export function index(req, res) {
  let condicion = verificarAcceso(req);
  return Administrarsocios.findAll({
    condicion,
    order: [['_id']],
    where: {
      estado: true
    }
  })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Administrarsocios from the DB
export function show(req, res) {
  return Administrarsocios.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Administrarsocios in the DB
export function create(req, res) {
  return Administrarsocios.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given Administrarsocios in the DB at the specified ID
export function upsert(req, res) {
  if (req.body._id) {
    //  delete req.body._id;
  }

  return Administrarsocios.upsert(req.body, {
    where: {
      _id: req.params.id
    }
  })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Administrarsocios in the DB
export function patch(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  if (req.body.saldo) {
    delete req.body.saldo;
  }
  return Administrarsocios.update(req.body, {
    where: {
      _id: req.params.id
    }
  })
    //   .then(handleEntityNotFound(res))
    //  .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Administrarsocios from the DB
export function destroy(req, res) {
  return Administrarsocios.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}


export function getAsignar(req, res) {
  return seqconsult.sequelize.query(`
    SELECT 
      (
        SELECT COUNT(*) 
        FROM "Solicitarplans" 
        WHERE "Solicitarplans".asignado_a = ASO._id AND estado_solicitud = 2 AND fecha > '2017-06-01'::date
      ) AS solicitudes_pendientes
      , NOW()::date - registro_en_bd::date AS dias_desde_el_ingreso
      , "_id"
      , nombres
      , apellidos
      , isapre
      , region_principal
      , region_secundaria
      , saldo
      , tarifa
      , tmp_suspendido
      , ASO.asignados_hoy
      , calculo.tasa_cierre
      , estado
    FROM "Administrarsocios" AS ASO
    LEFT JOIN (
    	SELECT *, 
        CASE WHEN (asigandos_mes::REAL - fallidos_actual::REAL) = 0 THEN 
          NULL 
        ELSE 
          (cerrados_actual::REAL/ (asigandos_mes::REAL - fallidos_actual::REAL)
      ) END AS tasa_cierre
	    FROM (
		    SELECT asignado_a
          , count(CASE WHEN (estado_solicitud IN (2,3,4)) THEN 1 END) AS asigandos_mes
          , count(CASE WHEN (estado_solicitud = 3) THEN 1 END) AS cerrados_actual
          , count(CASE WHEN (estado_solicitud = 4 AND razon_fallo IN (1,2,6,9,21)) THEN 1 END) AS fallidos_actual
        FROM "Solicitarplans" 
        WHERE (fecha >= NOW() - INTERVAL '3 month 2 weeks' AND fecha <= NOW() - INTERVAL '2 weeks') AND asignado_a IS NOT NULL 
        GROUP BY asignado_a
      ) AS calculo
    ) AS calculo ON calculo.asignado_a = ASO._id
    WHERE isapre = :isapre ${req.body.region ? ' AND (region_principal = :region OR region_secundaria = :region)' : ''}
    ORDER BY _id
   `, {
      replacements: {
        isapre: req.body.isapre,
        region: req.body.region
      },
      type: seqconsult.sequelize.QueryTypes.SELECT
    }).then(function (results) {
      return res.json(results);
    });
}

export function tasaCierre(req, res) {
  return seqconsult.sequelize.query(`
    SELECT id_socio, CASE WHEN (asigandos_mes::REAL - fallidos_actual::REAL) = 0 THEN NULL ELSE (cerrados_actual::REAL/ (asigandos_mes::REAL - fallidos_actual::REAL)) END AS tasa_cierre
    FROM (SELECT asignado_a AS id_socio, count(CASE WHEN (estado_solicitud IN (2,3,4)) THEN 1 END) AS asigandos_mes, 
        count(CASE WHEN (estado_solicitud = 3) THEN 1 END) AS cerrados_actual, 
        count(CASE WHEN (estado_solicitud = 4 AND razon_fallo IN (1,2,6,9,21)) THEN 1 END) AS fallidos_actual
        FROM "Solicitarplans" 
        WHERE (fecha >= NOW() - INTERVAL '3 month 2 weeks' AND fecha <= NOW() - INTERVAL '2 weeks') AND asignado_a IS NOT NULL 
        GROUP BY asignado_a) AS calculo
   `, {
      replacements: [],
      type: seqconsult.sequelize.QueryTypes.SELECT
    }).then(function (results) {
      return res.json(results);
    });
}

export function tasaCierreMovil(req, res) {
  return seqconsult.sequelize.query(`
    SELECT id_socio, CASE WHEN (asigandos_mes::REAL - fallidos_actual::REAL) = 0 THEN NULL ELSE (cerrados_actual::REAL/ (asigandos_mes::REAL - fallidos_actual::REAL)) END AS tasa_cierre
    FROM (SELECT asignado_a AS id_socio, count(CASE WHEN (estado_solicitud IN (2,3,4)) THEN 1 END) AS asigandos_mes, 
        count(CASE WHEN (estado_solicitud = 3) THEN 1 END) AS cerrados_actual, 
        count(CASE WHEN (estado_solicitud = 4 AND razon_fallo IN (1,2,6,9,21)) THEN 1 END) AS fallidos_actual
        FROM "Solicitarplans" 
        WHERE (fecha >= NOW() - INTERVAL '3 month 2 weeks' AND fecha <= NOW() - INTERVAL '2 weeks') AND asignado_a = :id_ejecutivo
        GROUP BY asignado_a) AS calculo
   `, {
      replacements: {
        id_ejecutivo: req.body.id
      },
      type: seqconsult.sequelize.QueryTypes.SELECT
    }).then(function (results) {
      return res.json(results);
    });
}

export function tasaCierreFija(req, res) {
  return seqconsult.sequelize.query(`
    SELECT id_socio, CASE WHEN (asigandos_mes::REAL - fallidos_actual::REAL) = 0 THEN NULL ELSE (cerrados_actual::REAL/ (asigandos_mes::REAL - fallidos_actual::REAL)) END AS tasa_cierre
    FROM (SELECT asignado_a AS id_socio, count(CASE WHEN (estado_solicitud IN (2,3,4)) THEN 1 END) AS asigandos_mes, 
        count(CASE WHEN (estado_solicitud = 3) THEN 1 END) AS cerrados_actual, 
        count(CASE WHEN (estado_solicitud = 4 AND razon_fallo IN (1,2,6,9,21)) THEN 1 END) AS fallidos_actual
        FROM "Solicitarplans" 
        WHERE (fecha >= NOW() - INTERVAL '3 month 2 weeks' AND 
          fecha <= to_date(to_char(current_date - INTERVAL '1 month', 'YYYY') || to_char(current_date - INTERVAL '1 month', 'MM') || '15', 'YYYYMMDD')) AND 
          asignado_a = :id_ejecutivo
        GROUP BY asignado_a) AS calculo
   `, {
      replacements: {
        id_ejecutivo: req.body.id
      },
      type: seqconsult.sequelize.QueryTypes.SELECT
    }).then(function (results) {
      return res.json(results);
    });
}

export function changePassword(req, res) {
  let socioId = req.params.id;
  let newPass = String(req.body.newPassword);

  Administrarsocios.belongsTo(User, { foreignKey: 'id_de_usuario', targetKey: '_id' });

  return Administrarsocios.find({
    where: {
      _id: socioId
    },
    include: [{ model: User }]
  })
    .then(administrarSocio => {
      let id_usuario = administrarSocio.User._id;
      return User.find({
        where: {
          _id: id_usuario
        }
      })
        .then(user => {
          user.password = newPass;
          return user.save()
            .then(() => {
              res.status(204).end();
            })
            .catch(() => {
              res.status(200).json({ "Error": "No se modifico la contraseña" });
            });
        });
    })
}
