'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newContacto;

describe('Contacto API:', function() {
  describe('GET /api/contactos', function() {
    var contactos;

    beforeEach(function(done) {
      request(app)
        .get('/api/contactos')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          contactos = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(contactos).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/contactos', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/contactos')
        .send({
          name: 'New Contacto',
          info: 'This is the brand new contacto!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newContacto = res.body;
          done();
        });
    });

    it('should respond with the newly created contacto', function() {
      expect(newContacto.name).to.equal('New Contacto');
      expect(newContacto.info).to.equal('This is the brand new contacto!!!');
    });
  });

  describe('GET /api/contactos/:id', function() {
    var contacto;

    beforeEach(function(done) {
      request(app)
        .get(`/api/contactos/${newContacto._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          contacto = res.body;
          done();
        });
    });

    afterEach(function() {
      contacto = {};
    });

    it('should respond with the requested contacto', function() {
      expect(contacto.name).to.equal('New Contacto');
      expect(contacto.info).to.equal('This is the brand new contacto!!!');
    });
  });

  describe('PUT /api/contactos/:id', function() {
    var updatedContacto;

    beforeEach(function(done) {
      request(app)
        .put(`/api/contactos/${newContacto._id}`)
        .send({
          name: 'Updated Contacto',
          info: 'This is the updated contacto!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedContacto = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedContacto = {};
    });

    it('should respond with the updated contacto', function() {
      expect(updatedContacto.name).to.equal('Updated Contacto');
      expect(updatedContacto.info).to.equal('This is the updated contacto!!!');
    });

    it('should respond with the updated contacto on a subsequent GET', function(done) {
      request(app)
        .get(`/api/contactos/${newContacto._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let contacto = res.body;

          expect(contacto.name).to.equal('Updated Contacto');
          expect(contacto.info).to.equal('This is the updated contacto!!!');

          done();
        });
    });
  });

  describe('PATCH /api/contactos/:id', function() {
    var patchedContacto;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/contactos/${newContacto._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Contacto' },
          { op: 'replace', path: '/info', value: 'This is the patched contacto!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedContacto = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedContacto = {};
    });

    it('should respond with the patched contacto', function() {
      expect(patchedContacto.name).to.equal('Patched Contacto');
      expect(patchedContacto.info).to.equal('This is the patched contacto!!!');
    });
  });

  describe('DELETE /api/contactos/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/contactos/${newContacto._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when contacto does not exist', function(done) {
      request(app)
        .delete(`/api/contactos/${newContacto._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
