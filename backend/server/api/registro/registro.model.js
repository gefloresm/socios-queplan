'use strict';

export default function (sequelize, DataTypes) {
    return sequelize.define('log_movil', {
        id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        tipo_peticion: DataTypes.STRING,
        endpoint: DataTypes.STRING,
        data: DataTypes.TEXT,
        id_usuario: DataTypes.INTEGER,
        id_player: DataTypes.STRING,
        id_device: DataTypes.STRING,
        fecha: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        ip: DataTypes.STRING,
        agente_usuario: DataTypes.STRING,
        info_app: DataTypes.STRING
    }, {
        schema: 'registros',
        tableName: 'log_movil'
    });
}