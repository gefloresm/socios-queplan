'use strict';

var express = require('express');
var controller = require('./recurso.controller');
var router = express.Router();

router.get('/uf', controller.index);

module.exports = router;
