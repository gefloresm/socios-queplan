'use strict';

import config from './environment';
import jwt from 'jsonwebtoken';
import lusca from 'lusca';
import { Registros } from '../sqldb';

let csrfExcludeAuth = '/auth';

export function csrfCondicionado(req, res, next) {
	let movil = false;
	if (req.headers.authorization && ((req.headers.authorization).split(" ")).length > 1) {
		jwt.verify((req.headers.authorization).replace("Bearer ", ""), config.secrets.session, (err, result) => {
			if (err) {
				req.headers.authorization = "";
				res.status(401).json({ "message": "Token no valido" });
			} else {
				let decoded = result;
				movil = (decoded.device === req.headers['content-id']);
				let tiempoExpiracion = decoded.exp;
				let ultimoAcceso = decoded.iat;
				if (movil && (tiempoExpiracion > ultimoAcceso)) {
					req.headers.authorization = "Bearer " + jwt.sign({ _id: decoded._id, role: decoded.role, device: decoded.device, playerId: decoded.playerId }, config.secrets.session, { expiresIn: 60 * 60 * 24 * 30 });
				}
				// Agregamos el id del dispositivo a al conjunto que la peticios para acceder a ella mientras se mueve en el servidor
				req.id_device = decoded.device;
				req.playerId = decoded.playerId;
				req.id_usuario = decoded._id;
				if (movil){
					// Forzar la desconexion del usuario en la aplicacion esta funcionando a partir de la version
					// 0.0.15. Aun falta definir un mensaje que se presenta al usuario en caso de ser necesario.
					/* if (decoded.appVersion !== '0.0.15') {
						console.log("\n appVersion: \n", decoded.appVersion);
						req.headers.authorization = "";
						return res.status(401).json({ "message": "Forzar actualizacion de version" });
					} */
					console.log(decoded);
					registrar(req, decoded);
				}

				if (movil) {
					return next();
				} else {
					lusca.csrf({ angular: true })(req, res, next);
				}
			}
		})
	} else {
		if (( ((req.path).includes(csrfExcludeAuth) || req.path == '/api/log/registrar') && !movil) || movil) {
			return next();
		} else {
			lusca.csrf({ angular: true })(req, res, next);
		}
	}
}

function registrar(req, decoded) {
    let ip = (req.headers['cf-connecting-ip'] ||
     req.headers['x-forwarded-for'] ||
     req.connection.remoteAddress ||
     req.socket.remoteAddress ||
	 req.connection.socket.remoteAddress).split(",")[0];

	let data = { params: req.params, body: req.body };

    return Registros.create({
        tipo_peticion: req.method,
        endpoint: req.originalUrl,
        data: JSON.stringify(data),
        id_usuario: decoded._id,
        id_player: decoded.playerId,
        id_device: decoded.device,
        ip: ip,
        agente_usuario: req.headers['user-agent'],
        info_app: decoded.appVersion
    })
}
