'use strict';

describe('Component: SaldosymovimientosComponent', function() {
  // load the controller's module
  beforeEach(module('sociosqueplanApp.saldosymovimientos'));

  var SaldosymovimientosComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    SaldosymovimientosComponent = $componentController('saldosymovimientos', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
