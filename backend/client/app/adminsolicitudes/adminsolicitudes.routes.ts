'use strict';

export default function ($stateProvider) {
    'ngInject';
    $stateProvider
        .state('adminsolicitudes', {
            url: '/adminsolicitudes',
            template: '<adminsolicitudes></adminsolicitudes>',
            authenticate: 'admin'
        })
        .state('solicitudesCB', {
            url: '/solicitudesCB',
            template: '<adminsolicitudes></adminsolicitudes>',
            authenticate: 'cb'
        });
}