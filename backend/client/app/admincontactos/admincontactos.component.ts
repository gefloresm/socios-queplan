'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './admincontactos.routes';

export class AdmincontactosComponent {
    /*@ngInject*/
    constructor($scope, $http, uiGridConstants) {
        'ngInject';

        $http.get('/api/contactos')
            .then(function (data) {
                $scope.gridContacto.data = data.data;
                console.log(data);
            })

        $scope.gridContacto = {
            enableFiltering: true,
            enableSorting: true,
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
                gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
            },
            columnDefs: [{
                    name: '_id',
                    enableCellEdit: false,
                    width: 60
                },
                {
                    name: 'origen',
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    width: 100,
                    cellFilter: 'mapOrigen_contacto',
                    editDropdownValueLabel: 'origen',
                    editDropdownOptionsArray: [
                        {
                            id: 1,
                            origen: 'Web'
                        },
                        {
                            id: 2,
                            origen: 'Teléfono'
                        },
                        {
                            id: 3,
                            origen: 'Email'
                        },
                        {
                            id: 98,
                            origen: 'Otro'
                        },
                        {
                            id: 99,
                            origen: 'Falso'
                        }
    ]
                },
                {
                    name: 'nombre',
                    width: 200
                },
                {
                    name: 'telefono',
                    width: 120
                },
                {
                    name: 'email',
                    width: 200
                },
                {
                    name: 'asignado_a',
                    type: 'number',
                    width: 80,
                    filter: {
                        condition: uiGridConstants.filter.EXACT
                    }
                },
                {
                    name: 'estado_contacto',
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    width: 100,
                    cellFilter: 'mapEstado_solicitud',
                    editDropdownValueLabel: 'estado',
                    editDropdownOptionsArray: [
                        {
                            id: 2,
                            estado: 'Asignada'
                        },
                        {
                            id: 4,
                            estado: 'Fallida'
                        },
                        {
                            id: 6,
                            estado: 'Duplicada'
                        },
                        {
                            id: 5,
                            estado: 'Eliminada'
                        },
                        {
                            id: 3,
                            estado: 'Cerrada'
                        },
                        {
                            id: 98,
                            estado: 'Cliente Falso'
                        },
                        {
                            id: 97,
                            estado: 'Pruebas QP'
                        },
                        {
                            id: 99,
                            estado: 'Otro'
                        }
                        ]
                },
                {
                    name: 'notas_admin',
                    width: 200
                },
                {
                    name: 'fecha',
                    enableCellEdit: false,
                    type: 'date',
                    cellFilter: 'date:"short"',
                    width: 120
                },
                {
                    name: 'mensaje'
                },
                {
                    name: 'datosingresados',
                    enableCellEdit: false
                },
                {
                    name: 'emailsocio',
                    enableCellEdit: false
                },
                {
                    name: 'emailcliente',
                    enableCellEdit: false
                }
                      ]


        }

        var promesaactualizar = function (row) {
            return $http.patch('/api/contactos/' + row._id, row);
        }

        $scope.saveRow = function (rowEntity) {
            console.log(rowEntity);
            $scope.gridApi.rowEdit.setSavePromise(rowEntity, promesaactualizar(rowEntity));
        };

        $scope.addData = function () {
            var n = $scope.gridContacto.data.length + 1;

            $http.post('/api/contactos', {
                "nombre": "Nombres",
                "email": "Email@",
                "mensaje": "Mensaje o info usuario",
                "telefono": "***",
                "estado_contacto": 1,
                "origen": 2,
                "notas_admin": ""
            }).then(function (data) {
                console.log(data);
                console.log("data.data");
                console.log(data.data);
                $scope.gridContacto.data.unshift(data.data);
                console.log("Info del Grid");
                console.log($scope.gridContacto.data);
            });

        };

    }
}

export default angular.module('sociosqueplanApp.admincontactos', [uiRouter])
    .config(routes)
    .component('admincontactos', {
        template: require('./admincontactos.html'),
        controller: AdmincontactosComponent,
        controllerAs: 'admincontactosCtrl'
    }).filter('mapOrigen_contacto', function () {
        var origenHash = {
            1: 'Web',
            2: 'Teléfono',
            3: 'Email',
            98: 'Otro',
            99: 'Falso'
        };
        return function (input) {
            if (!input) {
                return '';
            } else {
                return origenHash[input];
            }
        };
    })
    .name;